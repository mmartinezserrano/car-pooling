package com.miguelms.carpooling.service.impl;

import com.miguelms.carpooling.model.Car;
import com.miguelms.carpooling.repository.CarRepository;
import com.miguelms.carpooling.service.CarService;
import com.miguelms.carpooling.validator.CarValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CarServiceImpl implements CarService {

    @Autowired
    CarRepository carRepository;
    @Autowired
    CarValidator carValidator;

    @Override
    public void loadCars(Iterable <Car> cars) throws Exception {
        if (cars == null) throw new Exception("load 'cars' must be present");
        carValidator.validateCars(cars);
        deleteAll();
        for (Car car : cars) {
            car.setAvailable(true); // available by default
        }
        carRepository.saveAll(cars);
    }

    @Override
    public Iterable <Car> findAll() {
        return carRepository.findAll();
    }

    @Override
    public Optional <Car> findById(Long id){
        return carRepository.findById(id);
    }

    @Override
    public void deleteAll() {
        carRepository.deleteAll();
    }

    @Override
    public List <Car> findAvailableCars() {
        return carRepository.findAvailableCars();
    }

    @Override
    public void save(Car c) {
        carRepository.save(c);
    }
}
