package com.miguelms.carpooling.service.impl;

import com.miguelms.carpooling.model.Car;
import com.miguelms.carpooling.model.Group;
import com.miguelms.carpooling.model.Journey;
import com.miguelms.carpooling.service.CarService;
import com.miguelms.carpooling.service.GroupService;
import com.miguelms.carpooling.service.JourneyService;
import com.miguelms.carpooling.service.LocateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class LocateServiceImpl implements LocateService {
    @Autowired
    GroupService groupService;
    @Autowired
    JourneyService journeyService;
    @Autowired
    CarService carService;

    @Override
    public Optional <Car> locateGroup(Long groupId) {
        Group group = groupService.findById(groupId).orElse(null);
        if (group != null && group.getJourneyId() != null) {
            Journey journey = journeyService.findById(group.getJourneyId()).get();
            if (journey.getCompleted()) return Optional.empty();
            return Optional.of(carService.findById(journey.getCarId()).orElse(null));
        }

        return Optional.empty();
    }
}
