package com.miguelms.carpooling.service.impl;

import com.miguelms.carpooling.model.Car;
import com.miguelms.carpooling.model.Group;
import com.miguelms.carpooling.model.Journey;
import com.miguelms.carpooling.repository.JourneyRepository;
import com.miguelms.carpooling.service.CarService;
import com.miguelms.carpooling.service.GroupService;
import com.miguelms.carpooling.service.JourneyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class JourneyServiceImpl implements JourneyService {

    @Autowired
    JourneyRepository repository;
    @Autowired
    GroupService groupService;
    @Autowired
    CarService carService;

    @Override
    public Journey save(Journey journey) {
        return repository.save(journey);
    }

    @Override
    public Optional <Journey> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Optional <Journey> findByCarId(Long id) {
        return repository.findByCarId(id);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void delete(Journey journey) {
        repository.delete(journey);
    }

    @Override
    public void updateQueue() {
        List <Group> pendingGroups = groupService.findAllPending();
        for (Group pendingGroup : pendingGroups) {
            Car c = findCarFor(pendingGroup);
            if (c != null) {
                c.setAvailable(false);
                carService.save(c);
                Journey journey = new Journey()
                        .setCarId(c.getId())
                        .setPeople(pendingGroup.getPeople())
                        .setCompleted(false)
                        .setTimeStart(new Date());
                save(journey);

                pendingGroup.setJourneyId(journey.getId());
                pendingGroup.setPending(false);
                groupService.save(pendingGroup);
                log.info("Group: {}, asigned to journey: {}", pendingGroup.getId(), journey.getId());
                log.info("Group: {}", pendingGroup);
                log.info("Journey: {}", journey);
            }
        }

    }

    private Car findCarFor(Group group) {
        List <Car> cars = carService.findAvailableCars();
        for (Car car : cars) {
            if (group.getPeople() <= car.getSeats()) {
                return car;
            }
        }
        log.info("Car not found for group: {}", group);
        return null;
    }

    @Override
    public void completeJourney(Journey jour) {
        jour.setCompleted(true).setTimeEnd(new Date());
        save(jour);
        log.info("Journey completed: {}", jour);
        carService.findById(jour.getCarId()).ifPresent(car -> {
            car.setAvailable(true);
            carService.save(car);
            log.info("Car available: {}", car);
        });
    }
}
