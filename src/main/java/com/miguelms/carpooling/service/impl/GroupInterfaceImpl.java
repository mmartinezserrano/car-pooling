package com.miguelms.carpooling.service.impl;

import com.miguelms.carpooling.model.Group;
import com.miguelms.carpooling.repository.GroupRepository;
import com.miguelms.carpooling.service.CarService;
import com.miguelms.carpooling.service.GroupService;
import com.miguelms.carpooling.service.JourneyService;
import com.miguelms.carpooling.validator.GroupValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class GroupInterfaceImpl implements GroupService {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    JourneyService journeyService;
    @Autowired
    CarService carService;

    @Autowired
    GroupValidator validator;

    @Override
    public void add(Group group) throws Exception {
        validator.validateGroup(group);
        group.setRequestTime(new Date()).setPending(true);
        Group g = groupRepository.save(group);
        log.info("Saved group: {}", g);
        journeyService.updateQueue();
    }

    @Override
    public Optional <Group> findById(Long id) {
        return groupRepository.findById(id);
    }

    @Override
    public void delete(Long id) throws Exception {
        if (id == null) throw new Exception("Can't delete a journey without id");
        groupRepository.deleteById(id);
    }

    @Override
    public void delete(Group group) throws Exception {
        if (group == null) throw new Exception("Can't delete a null journey");
        groupRepository.delete(group);
    }

    @Override
    public int dropoff(Long id) throws Exception {
        if (id == null) throw new Exception("ID must be present");
        Group group = groupRepository.findById(id).orElse(null);
        if (group == null) return 0;

        // 1. If in journey -> mark as completed
        if(group.getJourneyId() != null)
            journeyService.findById(group.getJourneyId()).ifPresent(journey -> journeyService.completeJourney(journey));
        group.setPending(false);
        groupRepository.save(group);
        journeyService.updateQueue();
        return 1;
    }

    @Override
    public List <Group> findAllPending() {
        return groupRepository.findAllPending();
    }

    @Override
    public void save(Group group) {
        groupRepository.save(group);
    }
}
