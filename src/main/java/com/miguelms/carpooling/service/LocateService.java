package com.miguelms.carpooling.service;

import com.miguelms.carpooling.model.Car;

import java.util.Optional;

public interface LocateService {
    /**
     * Given a group ID such that ID=X, return
     *
     * @param groupId of the group
     * @return the car the group is traveling with, or no car if they are still waiting to be served.
     */
    Optional <Car> locateGroup(Long groupId);
}
