package com.miguelms.carpooling.service;

import com.miguelms.carpooling.model.Group;

import java.util.List;
import java.util.Optional;

public interface GroupService {
    void add(Group group) throws Exception;

    Optional <Group> findById(Long id);

    void delete(Long id) throws Exception;

    void delete(Group group) throws Exception;

    /**
     *
     * @param id, journey id.
     * @return 1 if group unregistered successfully
     *         0 if group not found
     */
    int dropoff(Long id) throws Exception;

    /**
     * Find groups with status "pending" == true
     * @return
     */
    List <Group> findAllPending();

    void save(Group group);
}
