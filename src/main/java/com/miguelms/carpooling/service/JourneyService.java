package com.miguelms.carpooling.service;

import com.miguelms.carpooling.model.Journey;

import java.util.Optional;

public interface JourneyService {

    Journey save(Journey journey) throws Exception;

    Optional <Journey> findById(Long id);

    Optional <Journey> findByCarId(Long id);

    void delete(Long id);

    void delete(Journey journey);

    /**
     * Assigns a journey with a Car to pending groups if it's available
     */
    void updateQueue();

    /**
     * Complete the journey passed by param
     * Mark journey as completed, register the time, and set the Car available for new journeys.
     * @param journey
     */
    void completeJourney(Journey journey);
}
