package com.miguelms.carpooling.service;

import com.miguelms.carpooling.model.Car;

import java.util.List;
import java.util.Optional;

public interface CarService {
    void loadCars(Iterable <Car> cars) throws Exception;

    Iterable <Car> findAll();

    Optional <Car> findById(Long id);

    void deleteAll();

    List<Car> findAvailableCars();

    void save(Car c);
}
