package com.miguelms.carpooling.model;

import lombok.*;
import lombok.experimental.Accessors;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
public class CarResponse {

    private Long id;

    private Integer seats;

    public void loadCar(Car car){
        this.setId(car.getId());
        this.setSeats(car.getSeats());
    }

}
