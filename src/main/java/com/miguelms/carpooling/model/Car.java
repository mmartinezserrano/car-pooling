package com.miguelms.carpooling.model;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
@Entity(name = "cars")
public class Car {
    @Id
    private Long id;

    private Integer seats;
    private Boolean available;

}
