package com.miguelms.carpooling.model;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@ToString
@Accessors(chain = true)
@Entity(name = "group_cars")
@AllArgsConstructor
public class Group {
    @Id
    private Long id;

    private Integer people;

    private Date requestTime;
    private Long journeyId;
    private Boolean pending;

}
