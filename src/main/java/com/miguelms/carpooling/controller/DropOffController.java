package com.miguelms.carpooling.controller;

import com.miguelms.carpooling.service.GroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class DropOffController {
    @Autowired
    GroupService groupService;

    /**
     * A group of people requests to be dropped off. Whether they traveled or not.
     *
     * @param param, ID=X, with ID of the group
     * @return 200 OK or 202 Accepted When the group is registered correctly
     * 400 Bad Request When there is a failure in the request format or the payload can't be unmarshalled.
     */
    @PostMapping(value = "dropoff", consumes = "application/x-www-form-urlencoded")
    public ResponseEntity dropOff(@RequestParam MultiValueMap <String, String> param) {
        if (!param.containsKey("ID"))
            return new ResponseEntity("", HttpStatus.BAD_REQUEST);
        log.info("requested drop off for group id: {}", param);
        try {
            int value = groupService.dropoff(Long.valueOf(param.getFirst("ID")));
            if (value == 1) {
                return new ResponseEntity("", HttpStatus.OK);
            } else if (value == 0) {
                return new ResponseEntity("", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("dropoff error: {}", e.getMessage());
            e.printStackTrace();
        }
        return new ResponseEntity("", HttpStatus.BAD_REQUEST);
    }

}
