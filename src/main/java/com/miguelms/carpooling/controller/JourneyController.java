package com.miguelms.carpooling.controller;

import com.miguelms.carpooling.model.Group;
import com.miguelms.carpooling.service.GroupService;
import com.miguelms.carpooling.service.JourneyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class JourneyController {
    @Autowired
    GroupService groupService;
    @Autowired
    JourneyService journeyService;

    /**
     * Group of people requests to perform a journey
     *
     * @param group, the group of people that wants to perform the journey
     * @return response:
     * 200 OK or 202 Accepted When the group is registered correctly
     * 400 Bad Request When there is a failure in the request format or the payload can't be unmarshalled.
     */
    @PostMapping(value = "journey", consumes = "application/json")
    public ResponseEntity journey(@RequestBody Group group) {
        try {
            log.info(group.toString());
            groupService.add(group);
            return new ResponseEntity("", HttpStatus.ACCEPTED);
        } catch (Exception e) {
            log.error("journey error: {}", e.getMessage());
            e.printStackTrace();
            return new ResponseEntity("", HttpStatus.BAD_REQUEST);
        }

    }

}
