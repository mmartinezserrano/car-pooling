package com.miguelms.carpooling.controller;

import com.miguelms.carpooling.model.Car;
import com.miguelms.carpooling.model.CarResponse;
import com.miguelms.carpooling.model.Group;
import com.miguelms.carpooling.service.GroupService;
import com.miguelms.carpooling.service.LocateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@Slf4j
public class LocateController {

    @Autowired
    LocateService locateService;
    @Autowired
    GroupService groupService;

    /**
     * @param param, expected ID=x, with the ID of the group. 'ID' uppercase
     * @return response: the car the group is travelling with, or no car if they're still waiting.
     *
     * 200 OK. With the car as the payload when the group is assigned to a car.
     * 204 No Content. When the group is waiting to be assigned to a car.
     * 404 Not Found. When the group is not to be found.
     * 400 Bad Request. When there is a failure in the request format or the payload can't be unmarshalled.
     */
    @PostMapping(value = "locate", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity locate(@RequestParam MultiValueMap <String, String> param) {
        log.info("params received: {}", param);
        if (!param.containsKey("ID"))
            return new ResponseEntity("", HttpStatus.BAD_REQUEST);
        try {
            Group g = groupService.findById(Long.valueOf(param.getFirst("ID"))).orElse(null);

            Optional <Car> car = locateService.locateGroup(Long.valueOf(param.getFirst("ID")));

            if (g != null && g.getPending())
                return new ResponseEntity("", HttpStatus.NO_CONTENT);
            if (g == null || !car.isPresent())
                return new ResponseEntity("", HttpStatus.NOT_FOUND);

            log.info("Locate: {}", car.get());
            return new ResponseEntity(new CarResponse(car.get().getId(), car.get().getSeats()), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Problem to parse ID as number: {}", param);
            return new ResponseEntity("", HttpStatus.BAD_REQUEST);
        }

    }
}
