package com.miguelms.carpooling.controller;

import com.miguelms.carpooling.model.Car;
import com.miguelms.carpooling.service.CarService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.HashMap;

@RestController
@Slf4j
public class CarsController {
    @Autowired
    CarService carService;

    /**
     * Load the list of available cars in the service and remove all previous data
     * (existing journeys and cars). This method may be called more than once during
     * the life cycle of the service.
     *
     * @param cars, json format, with "id" and "seats" values
     * @return response:
     * 200 OK When the list is registered correctly.
     * 400 Bad Request When there is a failure in the request format, expected headers,
     * or the payload can't be unmarshalled.
     */
    @PutMapping(value = "cars", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity cars(@RequestBody Collection <Car> cars) {
        try {
            carService.loadCars(cars);
            log.info("Cars loaded: {}", cars);
        } catch (Exception e) {
            log.error("cars error: {}", e.getMessage());
            return new ResponseEntity <>("", HttpStatus.BAD_REQUEST);
        }
        HashMap <String, String> responseMessage = new HashMap <>();
        responseMessage.put("message", String.format("%s cars loaded", cars.size()));
        return new ResponseEntity <>(responseMessage, HttpStatus.OK);
    }
}
