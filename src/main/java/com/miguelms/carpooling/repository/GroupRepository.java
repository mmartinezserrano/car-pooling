package com.miguelms.carpooling.repository;

import com.miguelms.carpooling.model.Group;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long> {

    @Query(value = "select * from group_cars where pending = true;", nativeQuery = true)
    List<Group> findAllPending();
}
