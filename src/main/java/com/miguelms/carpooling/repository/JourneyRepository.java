package com.miguelms.carpooling.repository;

import com.miguelms.carpooling.model.Journey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JourneyRepository extends CrudRepository<Journey, Long> {

    Optional<Journey> findByCarId(Long id);

}
