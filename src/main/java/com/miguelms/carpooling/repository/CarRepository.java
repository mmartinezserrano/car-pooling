package com.miguelms.carpooling.repository;

import com.miguelms.carpooling.model.Car;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {

    @Query(value = "select * from cars where available = true;", nativeQuery = true)
    List<Car> findAvailableCars();
}
