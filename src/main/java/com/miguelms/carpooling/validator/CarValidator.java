package com.miguelms.carpooling.validator;

import com.miguelms.carpooling.model.Car;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.util.stream.StreamSupport;

@Service
public class CarValidator {
    public void validateCar(Car car) throws Exception {
        if (car.getId() == null) throw new InvalidParameterException("Car ID must be present");
        if (car.getSeats() == null) throw new InvalidParameterException("Car seats must be present");
        if (car.getId() < 0) throw new IllegalArgumentException("Car ID must be greater or equal than 0");
        if (car.getSeats() < 4 || car.getSeats() > 6)
            throw new IllegalArgumentException("Car seats must be in 4-6 range");
    }

    public void validateCars(Iterable <Car> cars) throws Exception {
        long count = StreamSupport.stream(cars.spliterator(), false).count();
        if (count == 0) throw new IllegalArgumentException("No cars present");
        for (Car car : cars) {
            validateCar(car);
            checkUniqueId(car, cars);
        }
    }

    private void checkUniqueId(Car car, Iterable <Car> cars) {
        int counter = 0;
        for (Car car1 : cars) {
            if (car1.getId().equals(car.getId()))
                counter++;
        }
        if (counter > 1)
            throw new IllegalArgumentException("Car ID's mustn't be repeated");
    }
}
