package com.miguelms.carpooling.validator;

import com.miguelms.carpooling.model.Group;
import com.miguelms.carpooling.service.GroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;

@Service
@Slf4j
public class GroupValidator {
    @Autowired
    GroupService groupService;

    public void validateGroup(Group group) throws Exception {
        if (group == null) throw new InvalidParameterException("Journey can't be null");
        if (group.getId() == null) throw new IllegalArgumentException("Journey must have an ID");
        if (group.getId() < 0) throw new IllegalArgumentException("Journey ID must be greater or equal than 0");
        if (groupService.findById(group.getId()).isPresent()) throw new IllegalArgumentException("Group id must be unique");
        if (group.getPeople() == null) throw new IllegalArgumentException("Journey must have people");
        if (group.getPeople() < 1 || group.getPeople() > 6) throw new IllegalArgumentException("Journey's people must be between 1 and 6");
    }
}
