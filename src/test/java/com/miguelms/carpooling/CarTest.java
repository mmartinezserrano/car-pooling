package com.miguelms.carpooling;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CarTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void putCarsNoPresent() throws Exception {
        this.mockMvc.perform(put("/cars").contentType(MediaType.APPLICATION_JSON).content("[]"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void putCarsNoSeats() throws Exception {
        this.mockMvc.perform(put("/cars").contentType(MediaType.APPLICATION_JSON).content("[{\"id\": 1 }]"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void putCarsIdNegative() throws Exception {
        this.mockMvc.perform(put("/cars").contentType(MediaType.APPLICATION_JSON).content("[{\"id\": -1, \"seats\":4 }]"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void putCarsSeatsInvalid2() throws Exception {
        this.mockMvc.perform(put("/cars").contentType(MediaType.APPLICATION_JSON).content("[{\"id\": 1, \"seats\":2 }]"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void putCarsSeatsInvalid7() throws Exception {
        this.mockMvc.perform(put("/cars").contentType(MediaType.APPLICATION_JSON).content("[{\"id\": 1, \"seats\":7 }]"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void putCarOK() throws Exception {
        this.mockMvc.perform(put("/cars").contentType(MediaType.APPLICATION_JSON).content("[{\"id\": 1, \"seats\":4 }]"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void putCarsOK() throws Exception {
        this.mockMvc.perform(put("/cars").contentType(MediaType.APPLICATION_JSON)
                .content("[{\"id\": 1, \"seats\":4 },{\"id\": 2, \"seats\":5 },{\"id\": 3, \"seats\":6 }]"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void putCarsRepeated() throws Exception {
        this.mockMvc.perform(put("/cars").contentType(MediaType.APPLICATION_JSON)
                .content("[{\"id\": 1, \"seats\":4 },{\"id\": 1, \"seats\":5 },{\"id\": 3, \"seats\":6 }]"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }


}
