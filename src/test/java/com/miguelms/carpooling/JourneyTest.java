package com.miguelms.carpooling;

import com.miguelms.carpooling.model.Group;
import com.miguelms.carpooling.model.Journey;
import com.miguelms.carpooling.repository.GroupRepository;
import com.miguelms.carpooling.service.GroupService;
import com.miguelms.carpooling.service.JourneyService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Assert;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class JourneyTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    GroupService groupService;
    @Autowired
    JourneyService journeyService;
    @Autowired
    GroupRepository groupRepository;

    @Test
    public void postJourneyNoID() throws Exception {
        this.mockMvc.perform(post("/journey").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void postJourneyNoPeople() throws Exception {
        this.mockMvc.perform(post("/journey").contentType(MediaType.APPLICATION_JSON).content("{ \"id\": 1}"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void postJourneyNoPeopleInRange0() throws Exception {
        this.mockMvc.perform(post("/journey").contentType(MediaType.APPLICATION_JSON).content("{ \"id\": 1, \"people\": 0}"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void postJourneyNoPeopleInRange7() throws Exception {
        this.mockMvc.perform(post("/journey").contentType(MediaType.APPLICATION_JSON).content("{ \"id\": 1, \"people\": 7}"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void postJourneyOK() throws Exception {
        groupRepository.deleteAll();
        this.mockMvc.perform(post("/journey").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\": 1, \"people\":4 }"))
                .andDo(print())
                .andExpect(status().isAccepted());
    }

    @Test
    public void postDropoffOK() throws Exception {
        Group group = groupRepository.save(new Group().setPeople(4).setId(1l));
        Assert.isTrue(groupService.findById(group.getId()).isPresent(), "not created");

        this.mockMvc.perform(post("/dropoff").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("ID", String.valueOf(group.getId())))
                .andDo(print())
                .andExpect(status().isOk());

        Assert.isTrue(groupService.findById(group.getId()).get().getPending() == false, "still pending");
        if (group.getJourneyId() != null) {
            Journey jour = journeyService.findById(group.getJourneyId()).orElse(null);
            Assert.isTrue(jour.getCompleted(), "journey not completed");
        }

    }

    @Test
    public void postDropoffNotPresent() throws Exception {
        this.mockMvc.perform(post("/dropoff").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("ID", "999"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void postDropoffBadRequest() throws Exception {
        this.mockMvc.perform(post("/dropoff").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", ""))
                .andDo(print())
                .andExpect(status().isBadRequest());

    }


}
