package com.miguelms.carpooling.repositories;

import com.miguelms.carpooling.model.Group;
import com.miguelms.carpooling.repository.GroupRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
public class GroupRepositoryTest {
    @Autowired
    GroupRepository repository;

    @Test
    public void testFindPending(){
        repository.deleteAll();
        Group g = new Group().setPending(true).setPeople(4).setId(1l);
        Group g1 = new Group().setPending(false).setPeople(5).setId(2l);
        Group g2 = new Group().setPending(false).setPeople(2).setId(3l);
        repository.save(g);
        repository.save(g1);
        repository.save(g2);
        Assert.isTrue(repository.findAllPending().size() == 1, "pending not found");

    }
}
