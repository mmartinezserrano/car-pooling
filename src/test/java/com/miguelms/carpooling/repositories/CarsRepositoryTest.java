package com.miguelms.carpooling.repositories;

import com.miguelms.carpooling.model.Car;
import com.miguelms.carpooling.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
@Slf4j
public class CarsRepositoryTest {
    @Autowired
    CarRepository repository;

    @Test
    public void testFindPending() {
        repository.deleteAll();
        Car c = new Car().setAvailable(false).setId(1l);
        Car c1 = new Car().setAvailable(true).setId(2l);
        Car c2 = new Car().setAvailable(true).setId(3l);
        Car c3 = new Car().setAvailable(false).setId(4l);
        repository.save(c);
        repository.save(c1);
        repository.save(c2);
        repository.save(c3);
        log.info("cars: {}", repository.findAvailableCars());
        Assert.isTrue(repository.findAvailableCars().size() == 2, "cars available not found");

    }
}
