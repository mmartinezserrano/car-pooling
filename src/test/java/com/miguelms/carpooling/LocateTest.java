package com.miguelms.carpooling;

import com.miguelms.carpooling.model.Group;
import com.miguelms.carpooling.model.Journey;
import com.miguelms.carpooling.repository.CarRepository;
import com.miguelms.carpooling.repository.GroupRepository;
import com.miguelms.carpooling.repository.JourneyRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Assert;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Slf4j
public class LocateTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    JourneyRepository journeyRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    CarRepository carRepository;

    @Test
    public void testLocateOK() throws Exception {
        groupRepository.deleteAll();
        carRepository.deleteAll();
        journeyRepository.deleteAll();

        this.mockMvc.perform(put("/cars").contentType(MediaType.APPLICATION_JSON)
                .content("[{\"id\": 1, \"seats\":4 },{\"id\": 2, \"seats\":5 },{\"id\": 3, \"seats\":6 }]"))
                .andDo(print())
                .andExpect(status().isOk());


        this.mockMvc.perform(post("/journey").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\": 1, \"people\":6 }"))
                .andDo(print())
                .andExpect(status().isAccepted());


        log.info("groups: {}", groupRepository.findAll());
        Group group = groupRepository.findAll().iterator().next();
        Assert.isTrue(group.getPending() == false, "group still pending");
        Assert.isTrue(group.getPeople() == 6, "group people error");
        Assert.isTrue(group.getRequestTime() != null, "request time");


        Journey journey = journeyRepository.findAll().iterator().next();
        log.info("journey: {}", journey);
        Assert.isTrue(journey != null, "journey not created");
        Assert.isTrue(journey.getCompleted() == false, "journey not completed");
        Assert.isTrue(journey.getPeople() == 6, "journey people error");
        Assert.isTrue(journey.getTimeStart() != null, "journey timeStart error");
        Assert.isTrue(journey.getCarId() != null, "not carId assigned to journey");

        this.mockMvc.perform(post("/locate").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("ID", String.valueOf(group.getId())))
                .andDo(print())
                .andExpect(status().isOk());

        this.mockMvc.perform(post("/locate").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("ID", String.valueOf(99999)))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void testGroupWaiting() throws Exception {
        groupRepository.deleteAll();
        Group group = groupRepository.save(new Group().setPending(true).setPeople(4).setId(1l));

        this.mockMvc.perform(post("/locate").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("ID", String.valueOf(group.getId())))
                .andDo(print())
                .andExpect(status().isNoContent());

    }

    @Test
    public void testGroupNotFound() throws Exception {
        groupRepository.deleteAll();
        groupRepository.save(new Group().setId(2l).setPending(false).setPeople(3));
        this.mockMvc.perform(post("/locate").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("ID", String.valueOf(1)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGroupBadRequest() throws Exception {
        groupRepository.deleteAll();

        this.mockMvc.perform(post("/locate").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("abc", String.valueOf(1)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGroupIdLowerCase() throws Exception {
        groupRepository.deleteAll();
        groupRepository.save(new Group().setId(1l).setPending(false).setPeople(3));
        this.mockMvc.perform(post("/locate").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", String.valueOf(1)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}
