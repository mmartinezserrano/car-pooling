package com.miguelms.carpooling.acceptance;

import com.miguelms.carpooling.model.Group;
import com.miguelms.carpooling.model.Journey;
import com.miguelms.carpooling.repository.CarRepository;
import com.miguelms.carpooling.repository.GroupRepository;
import com.miguelms.carpooling.repository.JourneyRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Assert;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Slf4j
public class AcceptanceTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    JourneyRepository journeyRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    CarRepository carRepository;

    @Test
    public void testUseCase() throws Exception {
        journeyRepository.deleteAll();
        groupRepository.deleteAll();
        carRepository.deleteAll();

        this.mockMvc.perform(put("/cars").contentType(MediaType.APPLICATION_JSON)
                .content("[{\"id\": 1, \"seats\":4 },{\"id\": 2, \"seats\":5 },{\"id\": 3, \"seats\":6 }]"))
                .andDo(print())
                .andExpect(status().isOk());

        Assert.isTrue(carRepository.findAvailableCars().size() == 3, "cars available not found");

        this.mockMvc.perform(post("/journey").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\": 1, \"people\":6 }"))
                .andDo(print())
                .andExpect(status().isAccepted());

        Assert.isTrue(carRepository.findAvailableCars().size() == 2, "cars available not found");
        Assert.isTrue(groupRepository.findAllPending().size() == 0, "still pending");

        log.info("groups: {}", groupRepository.findAll());

        Group group = groupRepository.findAll().iterator().next();
        Assert.isTrue(group.getPending() == false, "group still pending");
        Assert.isTrue(group.getPeople() == 6, "group people error");
        Assert.isTrue(group.getRequestTime() != null, "request time");


        Journey journey = journeyRepository.findAll().iterator().next();
        log.info("journey: {}", journey);
        Assert.isTrue(journey != null, "journey not created");
        Assert.isTrue(journey.getCompleted() == false, "journey not completed");
        Assert.isTrue(journey.getPeople() == 6, "journey people error");
        Assert.isTrue(journey.getTimeStart() != null, "journey timeStart error");
        Assert.isTrue(journey.getCarId() != null, "not carId assigned to journey");

        Assert.isTrue(carRepository.findAvailableCars().size() == 2, "car still available");
        Assert.isTrue(group.getPending() == false, "group pending but mustn't");

        this.mockMvc.perform(post("/dropoff").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("ID", String.valueOf(group.getId())))
                .andDo(print())
                .andExpect(status().isOk());

        Assert.isTrue(journeyRepository.findById(journey.getId()).orElse(null).getCompleted(), "not completed");
        Assert.isTrue(carRepository.findById(journey.getCarId()).orElse(null).getAvailable(), "car not available");


    }
}
