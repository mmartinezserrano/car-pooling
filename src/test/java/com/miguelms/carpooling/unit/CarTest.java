package com.miguelms.carpooling.unit;

import com.miguelms.carpooling.model.Car;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class CarTest {


    @Test
    public void constructor(){
        Car c = new Car();
        Assert.isTrue(c != null, "car not created");

        Car c1 = new Car(1l, 4, true);
        Assert.isTrue(c1 != null, "car not created");
    }

    @Test
    public void testSettersGetters(){
        Car c = new Car();
        c.setId(2l).setAvailable(true).setSeats(5);
        Assert.isTrue(c.getId().equals(2l), "setter car failed");
        Assert.isTrue(c.getAvailable(), "setter car failed");
        Assert.isTrue(c.getSeats().equals(5), "setter car failed");
    }
}
