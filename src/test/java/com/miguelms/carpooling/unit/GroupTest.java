package com.miguelms.carpooling.unit;

import com.miguelms.carpooling.model.Group;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.util.Date;

public class GroupTest {

    @Test
    public void constructors() {
        Group g = new Group();
        Assert.isTrue(g != null, "group not created");
        Assert.isTrue(g.getPending() == null, "empty constructor assigns value");
        Assert.isTrue(g.getId() == null, "empty constructor assigns value");
        Assert.isTrue(g.getPeople() == null, "empty constructor assigns value");
        Assert.isTrue(g.getJourneyId() == null, "empty constructor assigns value");
        Assert.isTrue(g.getRequestTime() == null, "empty constructor assigns value");

        Group g1 = new Group(1l, 6, new Date(), 1l, false);
        Assert.isTrue(g1 != null, "group not created");
        Assert.isTrue(g1.getPending() != null, "empty constructor not assigns value");
        Assert.isTrue(g1.getId() != null, "empty constructor not assigns value");
        Assert.isTrue(g1.getPeople() != null, "empty constructor not assigns value");
        Assert.isTrue(g1.getJourneyId() != null, "empty constructor not assigns value");
        Assert.isTrue(g1.getRequestTime() != null, "empty constructor not assigns value");
    }


    @Test
    public void testSettersGetters(){
        Group g = new Group();
        g.setId(1l).setPending(true).setPeople(5).setJourneyId(1l).setRequestTime(new Date());

        Assert.isTrue(g.getId().equals(1l), "Group setter failed");
        Assert.isTrue(g.getPending(), "Group setter failed");
        Assert.isTrue(g.getRequestTime() != null, "Group setter failed");
        Assert.isTrue(g.getJourneyId().equals(1l), "Group setter failed");
        Assert.isTrue(g.getPeople().equals(5), "Group setter failed");
    }

}
