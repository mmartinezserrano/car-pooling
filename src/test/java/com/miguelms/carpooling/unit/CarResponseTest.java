package com.miguelms.carpooling.unit;

import com.miguelms.carpooling.model.Car;
import com.miguelms.carpooling.model.CarResponse;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class CarResponseTest {

    @Test
    public void emptyConstructorAndAccessProperties() {
        CarResponse c = new CarResponse();
        c.setId(1l);
        c.setSeats(4);

        Assert.isTrue(c != null, "car not created");
        Assert.isTrue(c.getSeats() == 4, "car seats not assigned");
        Assert.isTrue(c.getId() == 1l, "car id not assigned");
    }

    @Test
    public void carToString(){
        CarResponse c = new CarResponse();
        c.setId(1l);
        c.setSeats(4);

        Assert.isTrue(c.toString().contains("1"), "To string not contains id");
        Assert.isTrue(c.toString().contains("4"), "To string not contains seats");
    }

    @Test
    public void loaCars() {
        CarResponse c = new CarResponse();
        Car car = new Car(1l, 6, true);
        c.loadCar(car);

        Assert.isTrue(c.getId().equals(car.getId()), "Id not cloned");
        Assert.isTrue(c.getSeats().equals(car.getSeats()), "Seats not cloned");
    }
}
