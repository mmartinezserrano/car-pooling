package com.miguelms.carpooling.unit;

import com.miguelms.carpooling.model.Journey;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.util.Date;

public class JourneyTest {

    @Test
    public void constructors() {
        Journey j = new Journey(1l, 1l, 4, new Date(), new Date(), false);
        Assert.isTrue(j != null, "Journey all args constructor failed");
        Assert.isTrue(j.getId() != null, "Journey all args constructor failed");
        Assert.isTrue(j.getCompleted() != null, "Journey all args constructor failed");
        Assert.isTrue(j.getCarId() != null, "Journey all args constructor failed");
        Assert.isTrue(j.getPeople() != null, "Journey all args constructor failed");
        Assert.isTrue(j.getTimeStart() != null, "Journey all args constructor failed");
        Assert.isTrue(j.getTimeEnd() != null, "Journey all args constructor failed");

        Journey j1 = new Journey();
        Assert.isTrue(j1 != null, "Journey all args constructor failed");
    }

    @Test
    public void testSettersGetters() {
        Journey j = new Journey().setId(1l).setCarId(1l).setPeople(4)
                .setTimeStart(new Date()).setTimeEnd(new Date()).setCompleted(false);
        Assert.isTrue(j.getId().equals(1l), "Journey setter failed");
        Assert.isTrue(!j.getCompleted(), "Journey setter failed");
        Assert.isTrue(j.getCarId().equals(1l), "Journey setter failed");
        Assert.isTrue(j.getPeople().equals(4), "Journey setter failed");
        Assert.isTrue(j.getTimeStart() != null, "Journey setter failed");
        Assert.isTrue(j.getTimeEnd() != null, "Journey setter failed");
    }


}
