FROM maven:3.6.3-jdk-8

EXPOSE 9091

COPY ./ ./

RUN mvn clean package -Dhttps.protocols=TLSv1.2

ENTRYPOINT ["java","-jar","target/carpooling-0.0.1-SNAPSHOT.jar"]